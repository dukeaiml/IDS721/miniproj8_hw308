use clap::{App, Arg};
use csv::{ReaderBuilder, WriterBuilder};
use std::error::Error;
use std::fs::File;
use std::io::{self};

fn main() -> Result<(), Box<dyn Error>> {
    let matches = App::new("CSV to Uppercase")
        .version("1.0")
        .author("Your Name")
        .about("Reads a CSV file and converts all text to uppercase")
        .arg(Arg::with_name("INPUT")
            .help("Sets the input CSV file")
            .required(true)
            .index(1))
        .get_matches();

    let input_file = matches.value_of("INPUT").unwrap();
    println!("Processing file: {}", input_file);
    process_file(input_file)?;
    println!("Done!");
    Ok(())
}

fn process_file(path: &str) -> Result<(), Box<dyn Error>> {
    let file = match File::open(path) {
        Ok(file) => file,
        Err(err) => {
            println!("Failed to open file '{}': {}", path, err);
            return Err(err.into());
        }
    };

    let mut rdr = ReaderBuilder::new().has_headers(false).from_reader(file);
    let mut wtr = WriterBuilder::new().from_writer(io::stdout());

    let mut is_empty = true;
    for result in rdr.records() {
        let record = result?;
        is_empty = false;

        println!("Loop executed");

        // Print the original data
        println!("Before: {}", record.iter().collect::<Vec<&str>>().join(","));

        let processed_record: Vec<String> = record.iter().map(|field| field.to_uppercase()).collect();

        // Print the processed data
        println!("After: {}", processed_record.join(","));
        wtr.write_record(&processed_record)?;
    }

    if is_empty {
        println!("No records found in the file.");
    }

    Ok(())
}




#[cfg(test)]
mod tests {
    //use super::*;
    use std::{fs::{self, File}, io::Error as IoError};

    use csv::ReaderBuilder;

    fn process_file_and_return_output(file_path: &str) -> Result<String, IoError> {
        let file = File::open(file_path).map_err(|e| {
            println!("Failed to open file '{}': {:?}", file_path, e);
            e
        })?;
    
        let mut rdr = ReaderBuilder::new().has_headers(false).from_reader(file);
        let mut result = String::new();
        for record_result in rdr.records() {
            let record = record_result?;
            println!("Record read: {:?}", record);
            let uppercase_record = record.iter()
                .map(|s| s.to_uppercase())
                .collect::<Vec<_>>()
                .join(",");
            println!("Processed record: {}", uppercase_record);
            result.push_str(&uppercase_record);
            result.push('\n');
        }
    
        Ok(result)
    }

    #[test]
    fn test_uppercase_conversion_from_file() {
        let file_content = "This is a rainy day.\n";
        let file_path = "test.csv";
        fs::write(&file_path, file_content).expect("Failed to write test file");

        let file_content_read_back = fs::read_to_string(&file_path).expect("Failed to read test file back");
        assert_eq!(file_content, file_content_read_back);
        //println!("Input: {}", file_content_read_back);
        let result = process_file_and_return_output(&file_path);
        assert!(result.is_ok());
        let output = result.unwrap();

        assert_eq!(output, "THIS IS A RAINY DAY.\n");
        //println!("Output: {}", output);
        fs::remove_file(&file_path).expect("Failed to remove test file");
    }
}








