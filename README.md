# Rust Command-Line Tool with Testing
This project serves as a utility for reading, processing, and outputting CSV data, with a focus on transforming text data to uppercase, and is structured to facilitate both direct use and testing.


## Instructions
-  Run with command `cargo run -- path/to/the/file.csv`to execute the program.
-  Run `cargo test` for unit test.

## Core Funtionality
**1. Tool functionality**

It transforms all the text data in the CSV file to uppercase.

![](images/function.png)

**2. Data ingestion/processing**

Read CSV Files: The project includes functionality to open and read CSV files using the csv crate in Rust. This is handled by the ReaderBuilder from the csv crate, which facilitates parsing CSV data from a given file.

Data Processing: The main processing task performed on the CSV data is converting its content to uppercase. This is done record by record, where each field in the record is transformed to uppercase.

![](images/process.png)


**3. Testing implementation**

The project includes a test setup where a temporary CSV file is created with predefined content to test the functionality of converting the file’s text to uppercase.

The test validates that the file content is processed correctly and matches the expected output.

![](images/test.png)




